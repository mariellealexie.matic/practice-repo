package request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserInputRequest {
    private Character characterInput;
    private Integer sizeInput;
}
