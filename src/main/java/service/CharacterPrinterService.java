package service;

import request.UserInputRequest;

public interface CharacterPrinterService {
    void CharacterPrinter(UserInputRequest request) throws Exception;
}
