package service.impl;

import exception.ServiceException;
import service.CharacterPrinterService;
import request.UserInputRequest;
import module.patternprinter.PatternPrinter;

public class CharacterPrinterImpl extends PatternPrinter implements CharacterPrinterService {

    @Override
    public void CharacterPrinter(UserInputRequest request) throws Exception {
        this.printCharacterPatterns(request.getCharacterInput(), request.getSizeInput());
    }

    private void printCharacterPatterns(Character character, Integer size) throws Exception {

        // Condition based on the pre-defined selection of characters
        // If the character user input isn't from one of the few choices, throw an exception
        switch (character){
            case 'O' -> this.printOPattern(size);
            case 'X' -> this.printXPattern(size);
            case 'Y' -> this.printYPattern(size);
            case 'Z' -> this.printZPattern(size);
            default -> throw new Exception("Invalid character input. Only O, X, Y and Z are allowed.");
        }

        // If the integer is not a non-negative odd integer, throw an exception
        if (size % 2 == 0 || size < 0) {
            throw new ServiceException("No negative nor even integers are allowed.");
        }
    }
}